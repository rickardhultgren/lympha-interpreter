Installation
============

This is the documentation of the lympha-interpreter written in python. You can find documentation for the language itself on:

`http://lympha.readthedocs.io <http://lympha.readthedocs.io/>`_

There are to ways to install the interpreter:

 - download it from

	`https://gitlab.com/rickardhultgren/lympha-interpreter/blob/master/ <https://gitlab.com/rickardhultgren/lympha-interpreter/blob/master/>`_
 
 - or run
 	.. code-block:: none
		
		pip install lympha
 
 
For graphical rendering Graphviz is required:

	`https://www.graphviz.org/ <https://www.graphviz.org/>`_
