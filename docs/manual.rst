Manual
======

Depending on if you only download the interpreter, or if you install it on the entire system through e.g. ``pip`` you will call the programme by either ``./lympha`` or ``lympha``.

prompt syntax
-------------
.. code-block:: bash

	lympha -f <FILE NAME.lympha> -start <START STATEMENT> -steps <INTEGER> -graph -exe

.. csv-table:: "Arguments"
	:widths: 5, 20
	:header: "Argument", "Description"

	"``-f``", "In the current version of the interpreter, only one file is read and must hance contain both the algorithm and the patient data."
	"``-start``", "The name of the statement that should be considered as the first."
	"``-steps``", "The number of steps (``->``) beginning from 1."
	"``-graph``", "Graphical representation of statements, where statements with the value of 1 are yellow."
	"``-exe``", "For each statement with the value of 1, a prompt call with the same name as the statment is made."
