import setuptools

with open("README", "r") as fh:

    long_description = fh.read(),
    entry_points = {
        'console_scripts': ['lympha=lympha.command_line:main'],
    },
setuptools.setup(

     name='lympha',  

     version='1.0.1.1',

     scripts=['lympha'] ,

     author="Rickard Verner Hultgren",

     author_email="sababa.sababa@gmail.com",

     description="An interpreter for the language lympha",

     long_description=long_description,

     long_description_content_type="text/markdown",

     url="https://gitlab.com/rickardhultgren/lympha-interpreter",

     packages=setuptools.find_packages(),

     #classifiers=[
         #"Programming Language :: Python :: 3",
         #"OSI Approved :: BSD License",
         #"Operating System :: OS Independent",
     #],
     
     #uploaded with twine upload  dist/*
 )
